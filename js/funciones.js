/*--------------validar formulario--------------------------------*/
$(document).ready(function () {
  $("#formulario").validate({
    rules: {
      nombre: {
        required: true,
        minlength: 3,
      },

      email: {
        required: true,
        email: true,
      },

      asunto: {
        required: true,
      },

      telefono: {
        required: true,
        number: true,
      },

      mensaje: {
        required: true,
        minlength: 10,
      },
    },

    messages: {
      nombre: {
        minlength: "Debe incluir al menos 3 caracteres",
        required: "El campo nombre es requerido",
      },

      email: {
        email: "El mail debe tener el formato adecuado ejemplo@dominio.com",
        required: "El campo email es requerido",
      },

      asunto: {
        required: "Es necesario poner el motivo por el cual nos contactas",
      },

      telefono: {
        number: "Ingresa un número valido",
        required: "Es necesario que ingreses un numero de telefono/celular.",
      },

      mensaje: {
        required: "Ingresa el mensaje que quieres enviar.",
        minlength: "El mensaje debe ser mas largo",
      },
    },

    submitHandler: function (form) {
      form.submit();
    },
  });
});

/*--------------Efectos--------------------------------*/

ScrollReveal().reveal(".imagen-about-us", {
  origin: "left",
  delay: 600,
  duration: 1000,
  distance: "25rem" /*, reset: true */,
});
ScrollReveal().reveal(".texto-about-us", {
  origin: "right",
  delay: 600,
  duration: 1000,
  distance: "25rem",
});
ScrollReveal().reveal(".elemento-como-trabajamos", {
  origin: "bottom",
  delay: 600,
  duration: 1000,
  distance: "25rem",
});
ScrollReveal().reveal(".product", {
  origin: "top",
  delay: 600,
  duration: 1000,
  distance: "25rem",
});
ScrollReveal().reveal(".imagen", {
  origin: "left",
  delay: 600,
  duration: 1000,
  distance: "25rem" /*, reset: true */,
});
ScrollReveal().reveal("#botonCotizador", {
  origin: "right",
  delay: 600,
  duration: 1000,
  distance: "25rem",
});
ScrollReveal().reveal(".formulario-contact", {
  origin: "bottom",
  delay: 600,
  duration: 1000,
  distance: "25rem",
});
ScrollReveal().reveal(".imagen-contacto", {
  origin: "top",
  delay: 600,
  duration: 1000,
  distance: "25rem",
});
ScrollReveal().reveal("#accordion", {
  origin: "left",
  delay: 600,
  duration: 1000,
  distance: "25rem" /*, reset: true */,
});
ScrollReveal().reveal(".logo-milenium", {
  origin: "left",
  delay: 1000,
  duration: 1000,
  distance: "25rem",
});
ScrollReveal().reveal(".titulo", {
  origin: "top",
  delay: 600,
  duration: 1000,
  distance: "25rem",
});

/*--------------Contactanos--------------------------------*/
$("#accordion").accordion();

/*--------------Descripcion elementos--------------------------------*/
$(function () {
  $(document).tooltip();
});

/*--------------Boton -baja--------------------------------*/

$("#botonCotizador").click(function () {
  $("#calculadoraC").fadeToggle();
});

/*--------------Responsive menu--------------------------------*/
function responsiveMenu() {
  let menu = document.getElementById("links");
  if (menu.style.display === "block") {
    menu.style.display = "none";
  } else {
    menu.style.display = "block";
  }
}
